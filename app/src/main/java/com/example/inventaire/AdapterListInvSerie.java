package com.example.inventaire;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AdapterListInvSerie  extends BaseAdapter {

    private List<Article_Serie> listArticleSerie;

    private Activity activity;
    private ArrayList< HashMap<String, String>> data;
    private static LayoutInflater inflater = null;

    public AdapterListInvSerie(List<Article_Serie> listArticleSerie) {
        this.listArticleSerie = listArticleSerie;
    }


    public AdapterListInvSerie(Activity a, ArrayList<HashMap<String, String>> d) {
        activity = a;
        data = d;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (convertView == null)
            vi = inflater.inflate(R.layout.liste_article_serie, null);

        TextView code = (TextView) vi.findViewById(R.id.code_article_aff); //
        TextView num_serie = (TextView) vi.findViewById(R.id.num_serie_aff); //


        HashMap<String, String> song = new HashMap<String, String>();
        song = data.get(position);

        // Setting all values in listview
        code.setText(song.get(InSerie.KEY_CODE));
        num_serie.setText(song.get(InSerie.KEY_NUM_Serie));


        return vi;
    }

}
