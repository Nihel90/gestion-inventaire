package com.example.inventaire;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AdapterListInvLot  extends BaseAdapter {

    private List<Article_lot> listArticleLot;

    private Activity activity;
    private ArrayList< HashMap<String, String> > data;
    private static LayoutInflater inflater=null;

    public AdapterListInvLot(List<Article_lot> listArticleLot) {
        this.listArticleLot = listArticleLot;
    }



    public AdapterListInvLot(Activity a, ArrayList< HashMap<String, String> > d) {
        activity = a;
        data=d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.liste_artilce_lot, null);

        TextView code = (TextView)vi.findViewById(R.id.cab_article_aff); //
        TextView lot = (TextView)vi.findViewById(R.id.lot_article_aff); //
        TextView qte = (TextView)vi.findViewById(R.id.qte_article_aff); //

        HashMap<String, String> song = new HashMap<String, String>();
        song = data.get(position);

        // Setting all values in listview
        code.setText(song.get(InvLot.KEY_CODE));
        lot.setText(song.get(InvLot.KEY_LOT));
        qte.setText(song.get(InvLot.KEY_QUANTITE));

        return vi;
    }





}