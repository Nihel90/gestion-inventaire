package com.example.inventaire;

public class Article_Serie {

    int id;
    String code;
    String num_serie;


    // constructors
    public Article_Serie() {
    }

    public Article_Serie(int id, String code, String num_serie) {
        this.id = id;
        this.code = code;
        this.num_serie = num_serie;
     }


    // setters
    public void setId(int id) {
        this.id = id;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setNum_serie(String num_serie) {
        this.num_serie = num_serie;
    }



    // getters
    public int getId() {
        return this.id;
    }

    public String getCode() {
        return this.code;
    }

    public String getNumSerie() {
        return this.num_serie;
    }



}
