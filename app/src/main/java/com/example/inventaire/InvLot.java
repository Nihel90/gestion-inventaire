package com.example.inventaire;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import com.symbol.emdk.EMDKManager;
import com.symbol.emdk.EMDKResults;
import com.symbol.emdk.EMDKManager.EMDKListener;
import com.symbol.emdk.EMDKManager.FEATURE_TYPE;
import com.symbol.emdk.barcode.BarcodeManager;
import com.symbol.emdk.barcode.BarcodeManager.ConnectionState;
import com.symbol.emdk.barcode.BarcodeManager.ScannerConnectionListener;
import com.symbol.emdk.barcode.ScanDataCollection;
import com.symbol.emdk.barcode.Scanner;
import com.symbol.emdk.barcode.ScannerConfig;
import com.symbol.emdk.barcode.ScannerException;
import com.symbol.emdk.barcode.ScannerInfo;
import com.symbol.emdk.barcode.ScannerResults;
import com.symbol.emdk.barcode.ScanDataCollection.ScanData;
import com.symbol.emdk.barcode.Scanner.DataListener;
import com.symbol.emdk.barcode.Scanner.StatusListener;
import com.symbol.emdk.barcode.Scanner.TriggerType;
import com.symbol.emdk.barcode.StatusData.ScannerStates;
import com.symbol.emdk.barcode.StatusData;

public class InvLot extends Activity implements EMDKListener, DataListener, StatusListener, ScannerConnectionListener {

    private static final String TAG = InvLot.class.getSimpleName();

    static final String KEY_CODE = "code";
    static final String KEY_LOT = "lot";
    static final String KEY_QUANTITE = "quantite";

    private ListView list;

    private DatabaseHelper databaseHelper;

    private Article_lot article_lot;

    private EditText codeBarreLot;
    private EditText lot;
    private EditText quantiteLot;

    private CardView btnAjouter;

    AdapterListInvLot adapter;

    List<Article_lot> lArticle;
    // list of articles

    // contains the id of the item we are about to delete
    public int deleteItem;



    private EMDKManager emdkManager = null;
    private BarcodeManager barcodeManager = null;
    private Scanner scanner = null;

    private int scannerIndex = 0; // Keep the selected scanner
    private int defaultIndex = 0; // Keep the default scanner
    private int dataLength = 0;
    private String statusString = "";

    private boolean bSoftTriggerSelected = false;
    private boolean bDecoderSettingsChanged = false;
    private boolean bExtScannerDisconnected = false;
    private final Object lock = new Object();
    ArrayList<HashMap<String, String>> articleList;
    String focus="";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.invlot);

        databaseHelper = new DatabaseHelper(this);

        initViews();

        try {


            initListeners();
            getDataFromSQLite();

            EMDKResults results = EMDKManager.getEMDKManager(getApplicationContext(), this);
            if (results.statusCode != EMDKResults.STATUS_CODE.SUCCESS) {
                updateStatus("EMDKManager object request failed!");
                return;
            }
        }

        catch (Exception e)
        {
            Toast.makeText(InvLot.this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }

       }


    private void initViews() {

        try {


            codeBarreLot = (EditText) findViewById(R.id.code_barre_lot);
            lot = (EditText) findViewById(R.id.lot_article_lot);
            quantiteLot = (EditText) findViewById(R.id.quantite_lot);

            codeBarreLot.setOnFocusChangeListener(new View.OnFocusChangeListener()
            {

                @Override
                public void onFocusChange(View view, boolean b) {
                    focus="codeBarreLot";

                }
            });

            lot.setOnFocusChangeListener(new View.OnFocusChangeListener()
            {

                @Override
                public void onFocusChange(View view, boolean b) {
                    focus="lot";

                }
            });

            quantiteLot.setOnFocusChangeListener(new View.OnFocusChangeListener()
            {

                @Override
                public void onFocusChange(View view, boolean b) {
                    focus="quantiteLot";

                }
            });


            btnAjouter = (CardView) findViewById(R.id.btn_ajout_lot);

            article_lot = new Article_lot();

            list = (ListView) findViewById(R.id.list);


            quantiteLot.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                public boolean onEditorAction(TextView arg0, int arg1, KeyEvent arg2) {
                    //Do your operation here.

                    postDataToSQLite();
                    getDataFromSQLite();
                    return false;
                }
            });


        }
        catch (Exception e)
        {
            Toast.makeText(InvLot.this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }


    }



    private void initListeners()
    {
        btnAjouter.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                postDataToSQLite();
  //              getDataFromSQLite();
            }
        });

    }



    private  void getDataFromSQLite()
    {
       try {

           articleList = new ArrayList<HashMap<String, String>>();

           lArticle = databaseHelper.get_All_Article_lot();


           for (int i = 0; i < lArticle.size(); i++) {
               Article_lot currentArticle = lArticle.get(i);
               HashMap<String, String> map = new HashMap<String, String>();

               map.put(KEY_CODE, currentArticle.getCode());
               map.put(KEY_LOT, currentArticle.getLot() + "");
               map.put(KEY_QUANTITE, currentArticle.getQuantite() + "");


               articleList.add(map);
           }



           adapter=new AdapterListInvLot(this, articleList);
           list.setAdapter(adapter);

           list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

               @Override
               public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                              int pos, long arg3) {
                   deleteItem=pos;
                   // Creating a new alert dialog to confirm the delete
                   AlertDialog alert = new AlertDialog.Builder(arg1.getContext())
                           .setTitle("Etes vous sûr de supprimer le lot "+ "' "+lArticle.get(deleteItem).getLot()+" '")
                           .setPositiveButton("Oui",
                                   new DialogInterface.OnClickListener() {
                                       public void onClick(DialogInterface dialog,
                                                           int whichButton) {

                                           try {
                                               articleList.remove(deleteItem);
                                               databaseHelper.delete_article_lot(lArticle.get(deleteItem).getCode(), lArticle.get(deleteItem).getLot());
                                               lArticle = databaseHelper.get_All_Article_lot();

                                               adapter.notifyDataSetChanged();
                                               dialog.dismiss();
                                           }
                                           catch (Exception e)
                                           {
                                               Toast.makeText(InvLot.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                           }

                                       }
                                   })
                           .setNegativeButton("Non",
                                   new DialogInterface.OnClickListener() {
                                       public void onClick(DialogInterface dialog,
                                                           int whichButton) {
                                           // When you press cancel, just close the
                                           // dialog
                                           dialog.cancel();
                                       }
                                   }).show();

                   return false;
               }
           });

       }

       catch (Exception e)
       {
           Toast.makeText(InvLot.this, e.getMessage(), Toast.LENGTH_SHORT).show();
       }

    }



    private void postDataToSQLite (){

            try {


                String codeArticle = codeBarreLot.getText().toString().trim();
                String lotArtilce = lot.getText().toString().trim();
                String qteArticle = quantiteLot.getText().toString().trim();

                if (codeArticle.length() > 0) {

                        if (lotArtilce.length() > 0) {
                            if (!databaseHelper.checkCodeLotArticle(codeArticle, lotArtilce)) {


                                if (qteArticle.length() > 0) {

                                    int qteInv = Integer.parseInt(quantiteLot.getText().toString().trim());

                                    article_lot.setCode(codeArticle);
                                    article_lot.setLot(lotArtilce);
                                    article_lot.setQuantite(qteInv);

                                    long id = databaseHelper.Add_Article_lot(article_lot);

                                    if (id > 0) {
                                        Toast.makeText(InvLot.this, "L'article est inséré avec succès", Toast.LENGTH_LONG).show();
                                        codeBarreLot.requestFocus();
                                        codeBarreLot.setText("");
                                        lot.setText("");
                                        quantiteLot.setText("");
                                        getDataFromSQLite();

                                    } else {
                                        Toast.makeText(this, "Erreur d'insertion DB", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(this, "Veuillez insérer la quantité de cet article", Toast.LENGTH_SHORT).show();
                                    quantiteLot.requestFocus();
                                }

                            } else {
                                // créer AlertDialog pour confirmer l'ajout d'un couple code et lot existants

                                int qteInv = Integer.parseInt(quantiteLot.getText().toString().trim());

                                article_lot.setCode(codeArticle);
                                article_lot.setLot(lotArtilce);
                                article_lot.setQuantite(qteInv);
                                // Creating a new alert dialog to confirm the delete
                                AlertDialog alert = new AlertDialog.Builder(this)
                                        .setTitle("Code et lot existants, Souhaitez vous ajouter l'article?")
                                        .setPositiveButton("Oui",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog,
                                                                        int whichButton) {

                                                        long id = databaseHelper.Add_Article_lot(article_lot);

                                                        if (id > 0) {

                                                            Toast.makeText(InvLot.this, "L'article est inséré avec succès", Toast.LENGTH_LONG).show();
                                                            codeBarreLot.setText("");
                                                            lot.setText("");
                                                            quantiteLot.setText("");
                                                            codeBarreLot.requestFocus();
                                                            getDataFromSQLite();

                                                        }

                                                        adapter.notifyDataSetChanged();

                                                        dialog.dismiss();
                                                    }
                                                })
                                        .setNegativeButton("Non",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog,
                                                                        int whichButton) {
                                                        // When you press cancel, just close the
                                                        // dialog
                                                        dialog.cancel();
                                                    }
                                                }).show();

                                codeBarreLot.setText("");
                                lot.setText("");
                                quantiteLot.setText("");

                                codeBarreLot.requestFocus();

                            }
                        } else {
                            Toast.makeText(this, "Veuillez insérer le lot d'article", Toast.LENGTH_SHORT).show();
                            lot.requestFocus();
                        }
                    }



                else {
                        Toast.makeText(this, "Veuillez insérer le code article", Toast.LENGTH_SHORT).show();
                          codeBarreLot.requestFocus();
                    }


            } catch (Exception e) {
                Toast.makeText(InvLot.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }


        /*****/

        @Override
        public void onOpened(EMDKManager emdkManager) {
            updateStatus("EMDK open success!");
            this.emdkManager = emdkManager;
            // Acquire the barcode manager resources
            initBarcodeManager();
            // Enumerate scanner devices
            //enumerateScannerDevices();

            if ((scanner==null)) {
                scannerIndex = 0;
                bSoftTriggerSelected = false;
                bExtScannerDisconnected = false;
                deInitScanner();
                initScanner();
            }


        }

    @Override
    protected void onResume() {
        super.onResume();
        // The application is in foreground
        if (emdkManager != null) {
            // Acquire the barcode manager resources
            initBarcodeManager();
            // Enumerate scanner devices
            //enumerateScannerDevices();

            if ((scanner==null)) {
                scannerIndex = 0;
                bSoftTriggerSelected = false;
                bExtScannerDisconnected = false;
                deInitScanner();
                initScanner();
            }


            // Initialize scanner
            initScanner();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // The application is in background
        // Release the barcode manager resources
        deInitScanner();
        deInitBarcodeManager();
    }

    @Override
    public void onClosed() {
        // Release all the resources
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
        }
        updateStatus("EMDK closed unexpectedly! Please close and restart the application.");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Release all the resources
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
        }
    }

    @Override
    public void onData(ScanDataCollection scanDataCollection) {
        if ((scanDataCollection != null) && (scanDataCollection.getResult() == ScannerResults.SUCCESS)) {
            ArrayList <ScanData> scanData = scanDataCollection.getScanData();
            for(ScanData data : scanData) {
                //updateData("<font color='gray'>" + data.getLabelType() + "</font> : " + data.getData());
                updateData(data.getData());
            }
        }
    }

    @Override
    public void onStatus(StatusData statusData) {
        ScannerStates state = statusData.getState();
        switch(state) {
            case IDLE:
                statusString = statusData.getFriendlyName()+" is enabled and idle...";
                updateStatus(statusString);
                // set trigger type
                if(bSoftTriggerSelected) {
                    scanner.triggerType = TriggerType.SOFT_ONCE;
                    bSoftTriggerSelected = false;
                } else {
                    scanner.triggerType = TriggerType.HARD;
                }
                // set decoders
                if(bDecoderSettingsChanged) {
                    setDecoders();
                    bDecoderSettingsChanged = false;
                }
                // submit read
                if(!scanner.isReadPending() && !bExtScannerDisconnected) {
                    try {
                        scanner.read();
                    } catch (ScannerException e) {
                        updateStatus(e.getMessage());
                    }
                }
                break;
            case WAITING:
                statusString = "Scanner is waiting for trigger press...";
                updateStatus(statusString);
                break;
            case SCANNING:
                statusString = "Scanning...";
                updateStatus(statusString);
                break;
            case DISABLED:
                statusString = statusData.getFriendlyName()+" is disabled.";
                updateStatus(statusString);
                break;
            case ERROR:
                statusString = "An error has occurred.";
                updateStatus(statusString);
                break;
            default:
                break;
        }
    }

    @Override
    public void onConnectionChange(ScannerInfo scannerInfo, ConnectionState connectionState) {
        String status;
        String scannerName = "";
        String statusExtScanner = connectionState.toString();
        String scannerNameExtScanner = scannerInfo.getFriendlyName();
        /*if (deviceList.size() != 0) {
            scannerName = deviceList.get(scannerIndex).getFriendlyName();
        }*/
        if (scannerName.equalsIgnoreCase(scannerNameExtScanner)) {
            switch(connectionState) {
                case CONNECTED:
                    bSoftTriggerSelected = false;
                    synchronized (lock) {
                        initScanner();
                        bExtScannerDisconnected = false;
                    }
                    break;
                case DISCONNECTED:
                    bExtScannerDisconnected = true;
                    synchronized (lock) {
                        deInitScanner();
                    }
                    break;
            }
            status = scannerNameExtScanner + ":" + statusExtScanner;
            updateStatus(status);
        }
        else {
            bExtScannerDisconnected = false;
            status =  statusString + " " + scannerNameExtScanner + ":" + statusExtScanner;
            updateStatus(status);
        }
    }

    private void initScanner() {
        try {
            if (scanner == null) {
                //if ((deviceList != null) && (deviceList.size() != 0)) {
                // if (barcodeManager != null)
                scanner = barcodeManager.getDevice(BarcodeManager.DeviceIdentifier.DEFAULT);
            /*}
            else {
                updateStatus("Failed to get the specified scanner device! Please close and restart the application.");
                return;
            }*/
                if (scanner != null) {
                    scanner.addDataListener(this);
                    scanner.addStatusListener(this);
                    try {
                        scanner.enable();
                    } catch (ScannerException e) {
                        updateStatus(e.getMessage());
                        deInitScanner();
                    }
                } else {
                    updateStatus("Failed to initialize the scanner device.");
                }
            }
        }
        catch (Exception e)
        {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void deInitScanner() {
        if (scanner != null) {
            try{
                scanner.disable();
                scanner.release();
            } catch (Exception e) {
                updateStatus(e.getMessage());
            }
            scanner = null;
        }
    }

    private void initBarcodeManager(){
        barcodeManager = (BarcodeManager) emdkManager.getInstance(FEATURE_TYPE.BARCODE);
        // Add connection listener
        if (barcodeManager != null) {
            barcodeManager.addConnectionListener(this);
        }
    }

    private void deInitBarcodeManager(){
        if (emdkManager != null) {
            emdkManager.release(FEATURE_TYPE.BARCODE);
        }
    }

    private void setDecoders() {
        if (scanner != null) {
            try {
                ScannerConfig config = scanner.getConfig();
                // Set EAN8
                config.decoderParams.ean8.enabled = true;
                // Set EAN13
                config.decoderParams.ean13.enabled = true;
                // Set Code39
                config.decoderParams.code39.enabled= true;
                //Set Code128
                config.decoderParams.code128.enabled = true;
                scanner.setConfig(config);
            } catch (ScannerException e) {
                updateStatus(e.getMessage());
            }
        }
    }

    public void softScan(View view) {
        bSoftTriggerSelected = true;
        cancelRead();
    }

    private void cancelRead(){
        if (scanner != null) {
            if (scanner.isReadPending()) {
                try {
                    scanner.cancelRead();
                } catch (ScannerException e) {
                    updateStatus(e.getMessage());
                }
            }
        }
    }

    private void updateStatus(final String status){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //textViewStatus.setText("" + status);
                /** Code on update status*/
            }
        });
    }

    private void updateData(final String result){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (result != null) {

                    if(focus=="codeBarreLot")
                    {
                        codeBarreLot.setText(result);
                        lot.requestFocus();
                    }
                    else if(focus=="lot")
                    {
                        lot.setText(result);
                        quantiteLot.requestFocus();
                    }
                    else if(focus=="quantiteLot")
                    {
                        quantiteLot.setText(result);
                        postDataToSQLite();
                    }
                }

            }
        });
    }




}



