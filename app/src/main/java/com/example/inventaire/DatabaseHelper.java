package com.example.inventaire;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.nfc.Tag;
import android.util.Log;
import android.widget.Toast;

import java.io.Console;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


public class DatabaseHelper extends SQLiteOpenHelper {

       Context currentContext;
       // Database Version
       private static final int DATABASE_VERSION =103;

       // Database Name
       private static final String DATABASE_NAME = "Inventaire";

    // Logcat tag
    private static final String LOG = "DatabaseHelper";


       public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
           currentContext=context;
        }



    // Table Names
    private static final String TABLE_Article_lot = "Article_lot";
    private static final String TABLE_Article_serie = "Article_serie";



    // Common column names
    private static final String COLUMN_ID = "id";
    public static final String COLUMN_CODE ="code";


    //// Article_lot Table - column name
    public static final String COLUMN_LOT="lot";
    public static final String COLUMN_QTE= "qte";


    //// Article_num_serie Table - column names
    public static final String COLUMN_NUM_SERIE="num_serie";



    // Table Create Statements
    // Article_lot table create statement
    private String CREATE_TABLE_Article_lot = "CREATE TABLE "
            + TABLE_Article_lot + "(" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + COLUMN_CODE
            + " TEXT," + COLUMN_LOT + " TEXT," + COLUMN_QTE
            + " Integer" + ")";



    // Article_serie table create statement
    private static final String CREATE_TABLE_Article_serie = "CREATE TABLE "
            + TABLE_Article_serie + "(" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMN_CODE + " TEXT," + COLUMN_NUM_SERIE + " TEXT" + ")";



       @Override
       public void onCreate(SQLiteDatabase db) {
// creating required tables

           try {
               db.execSQL(CREATE_TABLE_Article_lot);
               db.execSQL(CREATE_TABLE_Article_serie);
           }

               catch (Exception e)
            {
                Toast.makeText(currentContext, e.getMessage(), Toast.LENGTH_SHORT).show();
            }

        }

        @Override
       public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

         // on upgrade drop older tables
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_Article_lot);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_Article_serie);

            onCreate(db);

        }


    /*
     * Creating an Article_lot
     */

    public long Add_Article_lot(Article_lot article_lot) {


        try {
             SQLiteDatabase db = this.getWritableDatabase();

             ContentValues values = new ContentValues();

             values.put(COLUMN_CODE, article_lot.getCode());
             values.put(COLUMN_LOT, article_lot.getLot());
             values.put(COLUMN_QTE, article_lot.getQuantite());

           // Inserting Row
             long id=db.insert(TABLE_Article_lot, null, values);
             db.close();
             return id;
            }


            catch(Exception e) {
                Toast.makeText(currentContext, e.getMessage(), Toast.LENGTH_SHORT).show();
                return  0;
             }

           }


    /*
     * get single Article_lot
     */
    public Article_lot get_Single_Article_Lot(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + TABLE_Article_lot + " WHERE "
                + COLUMN_ID + " = " + id + " ORDER BY" +id+" DESC";

        Log.e(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null)
            c.moveToFirst();

        Article_lot Al = new Article_lot();
        Al.setId(c.getInt(c.getColumnIndex(COLUMN_ID)));
        Al.setCode((c.getString(c.getColumnIndex(COLUMN_CODE))));
        Al.setLot(c.getString(c.getColumnIndex(COLUMN_LOT)));
        Al.setQuantite(c.getInt(c.getColumnIndex(COLUMN_QTE)));

        return Al;
    }


    /*
     * getting all Articles_lot
     * */

    public List<Article_lot> get_All_Article_lot() {

        List<Article_lot> ArticlesLot = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABLE_Article_lot + " ORDER BY " +COLUMN_ID+" DESC";

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                Article_lot Ar = new Article_lot();
                Ar.setId(c.getInt((c.getColumnIndex(COLUMN_ID))));
                Ar.setCode((c.getString(c.getColumnIndex(COLUMN_CODE))));
                Ar.setLot(c.getString(c.getColumnIndex(COLUMN_LOT)));
                Ar.setQuantite(c.getInt(c.getColumnIndex(COLUMN_QTE)));

                // adding to article_lot list
                ArticlesLot.add(Ar);
            }
            while (c.moveToNext());
        }
        db.close();

        return ArticlesLot;
    }



    /*
     * Updating an Article_lot
     */

    public int update_article_lot(Article_lot article_lot) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_CODE, article_lot.getCode());
        values.put(COLUMN_LOT, article_lot.getLot());
        values.put(COLUMN_QTE, article_lot.getQuantite());

        // updating row
        return db.update(TABLE_Article_lot, values, COLUMN_ID + " = ?",
                new String[] { String.valueOf(article_lot.getId()) });
    }


    /*
     * Deleting an Article_lot
     */
    public void delete_article_lot(String cab,String lot) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_Article_lot,COLUMN_CODE + " = ? AND " + COLUMN_LOT + " = ?",
                new String[] {cab, lot});
    }




    /*
     * Creating article_serie
     */

    public void delete_table_articleSerie()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+ TABLE_Article_serie );
        db.close();
    }

    public void delete_table_articleLot()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+ TABLE_Article_lot );
        db.close();
    }




    public long Add_Article_serie(Article_Serie article_serie) {

       try {
           SQLiteDatabase db = this.getWritableDatabase();

           ContentValues values = new ContentValues();
           values.put(COLUMN_CODE, article_serie.getCode());
           values.put(COLUMN_NUM_SERIE, article_serie.getNumSerie());

           // insert roW
           long  id = db.insert(TABLE_Article_serie, null, values);
           db.close();

           return id;
       }
       catch (Exception e) {
            Toast.makeText(currentContext, e.getMessage(), Toast.LENGTH_SHORT).show();
            return  0;
        }
    }




    /*
     * get single Article_serie
     */
    public Article_Serie get_Single_Article_Serie(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + TABLE_Article_serie + " WHERE "
                + COLUMN_ID + " = " + id;

        Log.e(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null)
            c.moveToFirst();

        Article_Serie As = new Article_Serie();
        As.setId(c.getInt(c.getColumnIndex(COLUMN_ID)));
        As.setCode((c.getString(c.getColumnIndex(COLUMN_CODE))));
        As.setNum_serie(c.getString(c.getColumnIndex(COLUMN_NUM_SERIE)));


        return As;
    }




    /**
     * getting all articles_serie
     * */
    public List<Article_Serie> get_All_Article_serie() {
        List<Article_Serie> Articles_serie = new ArrayList<Article_Serie>();
        String selectQuery = "SELECT  * FROM " + TABLE_Article_serie + " ORDER BY " +COLUMN_ID+" DESC";

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                Article_Serie As = new Article_Serie();
                As.setId(c.getInt((c.getColumnIndex(COLUMN_ID))));
                As.setCode((c.getString(c.getColumnIndex(COLUMN_CODE))));
                As.setNum_serie(c.getString(c.getColumnIndex(COLUMN_NUM_SERIE)));


                // adding to article_serie list
                Articles_serie.add(As);
            } while (c.moveToNext());
        }

        return Articles_serie;
    }


    public List<Article_Serie> get_Filtred_Article_serie_by_serie_code(String code) {
        List<Article_Serie> Articles_serie = new ArrayList();
        String selectQuery = "SELECT  * FROM "+TABLE_Article_serie+ " WHERE " +
                 COLUMN_CODE + " = '" + code + "'" + "ORDER BY " + COLUMN_ID + " DESC ";




        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                Article_Serie As = new Article_Serie();
                As.setId(c.getInt((c.getColumnIndex(COLUMN_ID))));
                As.setCode((c.getString(c.getColumnIndex(COLUMN_CODE))));
                As.setNum_serie(c.getString(c.getColumnIndex(COLUMN_NUM_SERIE)));


                // adding to article_serie list
                Articles_serie.add(As);
            } while (c.moveToNext());
        }

        return Articles_serie;
    }


    public int update_article_serie(Article_Serie article_serie) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_CODE, article_serie.getCode());
        values.put(COLUMN_NUM_SERIE, article_serie.getNumSerie());


        // updating row
        return db.update(TABLE_Article_serie, values, COLUMN_ID + " = ?",
                new String[] { String.valueOf(article_serie.getId()) });
    }


    /*
     * Deleting an Article_lot
     */
    public void delete_article_serie(String numSerie) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_Article_serie, COLUMN_NUM_SERIE + " = ?",
                new String[] {numSerie});
    }




    public boolean checkCodeLotArticle(String code, String lot) {

        // array of columns to fetch
        String[] columns = {
                COLUMN_ID
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        String selection = COLUMN_CODE + " = ?" + " AND " + COLUMN_LOT + " = ?";

        // selection arguments
        String[] selectionArgs = {code, lot};

        // query article_lot table with conditions
        /**
         * Here query function is used to fetch records from article_lot  table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT id FROM article_lot WHERE code = 'bblablabla' AND lot = 'blablakfj';
         */
        Cursor cursor = db.query(TABLE_Article_lot, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order

        int cursorCount = cursor.getCount();

        cursor.close();
        db.close();
        if (cursorCount > 0) {
            return true;
        }

        return false;
    }


    public boolean checkNumSerie(String code) {

        // array of columns to fetch
        String[] columns = {
                COLUMN_ID
        };
        SQLiteDatabase db = this.getReadableDatabase();

        // selection criteria
        String selection = COLUMN_NUM_SERIE + " = ?";

        // selection argument
        String[] selectionArgs = {code};

        // query user table with condition
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
         */
        Cursor cursor = db.query(TABLE_Article_serie, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                      //filter by row groups
                null);                      //The sort order
        int cursorCount = cursor.getCount();
        cursor.close();
        db.close();

        if (cursorCount > 0) {
            return true;
        }

        return false;
    }



    public boolean EmptyArticleLot () {
        //    String count = "SELECT count(*) FROM "+ TABLE_Article_serie;

        SQLiteDatabase db = this.getWritableDatabase();
        boolean empty = true;
        Cursor cur = db.rawQuery("SELECT COUNT(*) FROM "+ TABLE_Article_lot, null);
        if (cur != null && cur.moveToFirst()) {
            empty = (cur.getInt (0) == 0);
        }
        cur.close();
        db.close();

        return empty;

    }


    public boolean EmptyArticleSerie () {
        //    String count = "SELECT count(*) FROM "+ TABLE_Article_serie;

        SQLiteDatabase db = this.getWritableDatabase();
        boolean empty = true;
        Cursor cur = db.rawQuery("SELECT COUNT(*) FROM "+ TABLE_Article_serie, null);
        if (cur != null && cur.moveToFirst()) {
            empty = (cur.getInt (0) == 0);
        }
        cur.close();
        db.close();

        return empty;

    }





}
