package com.example.inventaire;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Checkable;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Export extends Activity {

    CardView btnSaveExport;
    CheckBox checkInvSerie,CheckInvLot;
    private DatabaseHelper databaseHelper;

    private EditText mail;

    private static final int REQUEST_ID_READ_PERMISSION = 100;
    private static final int REQUEST_ID_WRITE_PERMISSION = 200;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.export);

        mail = (EditText) findViewById(R.id.email);
        btnSaveExport=(CardView) findViewById(R.id.btnExport) ;

        btnSaveExport.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                saveExport();

            }
        });

        checkInvSerie=(CheckBox) findViewById(R.id.checkInvSerie);
        CheckInvLot=(CheckBox) findViewById(R.id.checkInvLot);

        databaseHelper = new DatabaseHelper(Export.this);

    }

    private void askPermissionAndWriteFile(String filecontents, String fileName) {
        boolean canWrite = this.askPermission(REQUEST_ID_WRITE_PERMISSION,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        //
        if (canWrite) {
            this.writeFile(filecontents, fileName);
        }
    }



    // With Android Level >= 23, you have to ask the user
    // for permission with device (For example read/write data on the device).
    private boolean askPermission(int requestId, String permissionName) {
        if (android.os.Build.VERSION.SDK_INT >= 23) {

            // Check if we have permission
            int permission = ActivityCompat.checkSelfPermission(this, permissionName);

            if (permission != PackageManager.PERMISSION_GRANTED) {
                // If don't have permission so prompt the user.
                this.requestPermissions(
                        new String[]{permissionName},
                        requestId
                );
                return false;
            }
        }
        return true;
    }


    public  void saveExport()

    {
        try {

            String adresse = mail.getText().toString().trim();

            if (adresse.length()==0) {

                // Creating a new alert dialog to export file
                AlertDialog alert = new AlertDialog.Builder(this)
                        .setTitle("Souhaitez-vous exporter les fichiers")
                        .setPositiveButton("Par Mail",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                        dialog.dismiss();
                                        Toast.makeText(Export.this, "Veuillez saisir votre adresse mail", Toast.LENGTH_LONG).show();
                                        mail.requestFocus();

                                    }
                                })
                        .setNegativeButton("Sur Terminal",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        // When you press cancel, just close the
                                        // dialog
                                        dialog.cancel();


                                        String fileNameSerie = "InvSérie.csv";
                                        String fileContents = "";
                                        List<Article_Serie> dataSqlS = getAllInvSerieFromDB();
                                        for (int i = 0; i < dataSqlS.size(); i++) {
                                            fileContents += dataSqlS.get(i).code + " ; " + dataSqlS.get(i).num_serie + "\n";
                                        }


                                        String fileNameLot = "InvLot.csv";
                                        String fileC = "";
                                        List<Article_lot> dataSqlL = getAllInvLotFromDB();
                                        for (int i = 0; i < dataSqlL.size(); i++) {
                                            fileC += dataSqlL.get(i).code + " ; " + dataSqlL.get(i).lot + " ; " + dataSqlL.get(i).quantite + " \n";
                                        }


                                       try {
                                            String rootPath = Environment.getExternalStorageDirectory()
                                                    .getAbsolutePath() + "/Inventaire/";
                                            File root = new File(rootPath);
                                            if (!root.exists()) {
                                                root.mkdirs();
                                            }
                                            File f1 = new File(rootPath + fileNameSerie);
                                            File f2 = new File(rootPath + fileNameLot);

                                           if (f1.exists()) {
                                                f1.delete();
                                            }
                                            f1.createNewFile();

                                           if (f2.exists()) {
                                               f2.delete();
                                           }
                                            f2.createNewFile();


                                           FileOutputStream out1 = new FileOutputStream(f1);
                                           FileOutputStream out2 = new FileOutputStream(f2);


                                           OutputStreamWriter myOutWriter1 = new OutputStreamWriter(out1);
                                           OutputStreamWriter myOutWriter2 = new OutputStreamWriter(out2);

                                           myOutWriter1.append(fileContents);
                                           myOutWriter2.append(fileC);

                                           myOutWriter1.close();
                                           myOutWriter2.close();

                                           out1.close();
                                           out2.close();

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        Toast.makeText(Export.this, "Les fichiers sont enregistrés sous le répertoire 'Inventaire'", Toast.LENGTH_LONG).show();
                                    }
                                })
                        .show();



            } else {

                if (!checkInvSerie.isChecked() && !CheckInvLot.isChecked()) {
                    Toast.makeText(this, "Merci de choisir : Inventaire/Série ou Inventaire/Lot", Toast.LENGTH_SHORT).show();
                } else {

                    try {

                        if (isExternalStorageReadable()) {

                            if ((checkInvSerie.isChecked()) && (CheckInvLot.isChecked())) {
                                ///Create file: Inv_serie and File: Inv_lot

                                String fileNameSerie = "Inventaire_Série.csv";
                                String fileContents = "";
                                List<Article_Serie> dataSqlS = getAllInvSerieFromDB();
                                for (int i = 0; i < dataSqlS.size(); i++) {
                                    fileContents += dataSqlS.get(i).code + " ; " + dataSqlS.get(i).num_serie + "\n";
                                }
                                askPermissionAndWriteFile(fileContents, fileNameSerie);

                                String fileNameLot = "Inventaire_Lot.csv";
                                String fileC = "";
                                List<Article_lot> dataSqlL = getAllInvLotFromDB();
                                for (int i = 0; i < dataSqlL.size(); i++) {
                                    fileC += dataSqlL.get(i).code + " ; " + dataSqlL.get(i).lot + " ; " + dataSqlL.get(i).quantite + " \n";
                                }
                                askPermissionAndWriteFile(fileC, fileNameLot);


                                ArrayList<Uri> uris = new ArrayList<Uri>();
                                File dirSerie = Environment.getExternalStorageDirectory();
                                File fileSerie = new File(dirSerie, "Inventaire_Série.csv");
                                Uri pathSerie = Uri.fromFile(fileSerie);
                                uris.add(pathSerie);

                                File dirLot = Environment.getExternalStorageDirectory();
                                File fileLot = new File(dirLot, "Inventaire_Lot.csv");
                                Uri pathLot = Uri.fromFile(fileLot);
                                uris.add(pathLot);

                                Intent email = new Intent(Intent.ACTION_SEND_MULTIPLE);
                                email.putExtra(Intent.EXTRA_SUBJECT, "Inventaire");
                                email.putExtra(Intent.EXTRA_TEXT, "Vous trouverez ci joint les deux fichiers: Inventaire_série.csv  et Inventaire_lot.csv");
                                email.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{adresse});
                                email.setType("text/plain");
                                email.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
                                startActivity(Intent.createChooser(email, "Send Text File"));

                            } else if (checkInvSerie.isChecked()) {
                                String fileNameSerie = "Inventaire_Série.csv";
                                String fileContents = "";
                                List<Article_Serie> dataSql = getAllInvSerieFromDB();
                                for (int i = 0; i < dataSql.size(); i++) {
                                    fileContents += dataSql.get(i).code + " ; " + dataSql.get(i).num_serie + "\n";
                                }

                                askPermissionAndWriteFile(fileContents, fileNameSerie);


                                File dir = Environment.getExternalStorageDirectory();
                                File file = new File(dir, "Inventaire_Série.csv");
                                Uri path = Uri.fromFile(file);

                                Intent email = new Intent(Intent.ACTION_SEND);
                                email.putExtra(Intent.EXTRA_SUBJECT, "Inventaire");
                                email.putExtra(Intent.EXTRA_TEXT, "Vous trouverez ci joint le fichier: Inventaire_série.csv");
                                email.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{adresse});
                                email.putExtra(Intent.EXTRA_STREAM, path);
                                email.setType("message/rfc822");
                                startActivity(Intent.createChooser(email, "Send Text File"));


                            } else if (CheckInvLot.isChecked()) {


                                String fileNameLot = "Inventaire_Lot.csv";
                                String fileC = "";
                                List<Article_lot> dataSql = getAllInvLotFromDB();
                                for (int i = 0; i < dataSql.size(); i++) {
                                    fileC += dataSql.get(i).code + " ; " + dataSql.get(i).lot + " ; " + dataSql.get(i).quantite + " \n";
                                }
                                askPermissionAndWriteFile(fileC, fileNameLot);


                                File dir = Environment.getExternalStorageDirectory();
                                File file = new File(dir, "Inventaire_Lot.csv");
                                Uri path = Uri.fromFile(file);

                                Intent email = new Intent(Intent.ACTION_SEND);
                                email.putExtra(Intent.EXTRA_SUBJECT, "Inventaire");
                                email.putExtra(Intent.EXTRA_TEXT, "Vous trouverez ci joint le fichier: Inventaire_lot.csv");
                                email.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{adresse});
                                email.putExtra(Intent.EXTRA_STREAM, path);
                                email.setType("message/rfc822");
                                startActivity(Intent.createChooser(email, "Send Text File"));

                            }


                        } else {
                            Toast.makeText(this, "Erreur écriture", Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        Toast.makeText(Export.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }

            }
        }

        catch (Exception e) {
            Toast.makeText(Export.this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }





    private List<Article_lot> getAllInvLotFromDB() {

        List<Article_lot> lArticle=new ArrayList<Article_lot>();
        try {
            lArticle = databaseHelper.get_All_Article_lot();
            return lArticle;
        }
        catch (Exception e)
        {
            Toast.makeText(Export.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            return lArticle;
        }


    }


    private void writeFile(String fileContents, String fileName) {

        File extStore = Environment.getExternalStorageDirectory();
        // ==> /storage/emulated/0/note.txt

        String path = extStore.getAbsolutePath() + "/" + fileName;


        try {
            File myFile = new File(path);
            myFile.createNewFile();
            FileOutputStream fOut = new FileOutputStream(myFile);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
            myOutWriter.append(fileContents);
            myOutWriter.close();
            fOut.close();

       //     Toast.makeText(getApplicationContext(), fileName + " est enregistré avec succès", Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    private  List<Article_Serie> getAllInvSerieFromDB()
    {
        List<Article_Serie> lArticle=new ArrayList<Article_Serie>();
        try {
            lArticle = databaseHelper.get_All_Article_serie();
            return lArticle;
        }
        catch (Exception e)
        {
            Toast.makeText(Export.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            return lArticle;
        }

    }

    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }



    public static Boolean isAppAvailable(Context context, String appName)
    {
        PackageManager pm = context.getPackageManager();
        boolean isInstalled;
        try {
            pm.getPackageInfo(appName,PackageManager.GET_ACTIVITIES);
            isInstalled = true;
        } catch (PackageManager.NameNotFoundException e) {
            isInstalled = false;
        }
        return isInstalled;
    }

    public void sendMail(Context context, String mailID, String subject, File attachment, Uri uri)
    {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(Intent.EXTRA_EMAIL, mailID);
        // Need to grant this permission
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        // Attachment
        intent.setType("vnd.android.cursor.dir/email");

        if (attachment != null)
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(attachment));
        else if (uri != null)
            intent.putExtra(Intent.EXTRA_STREAM, uri);

        if (!TextUtils.isEmpty(subject))
            intent.putExtra(Intent.EXTRA_SUBJECT, subject);


        if (isAppAvailable(context, "com.google.android.gm"))
                intent.setPackage("com.google.android.gm");
            startActivityForResult(intent, 101);

    }




}
