package com.example.inventaire;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    CardView bLot, bSerie, bExport, bapropos;
    Button bExit;
    private DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        bLot = (CardView) findViewById(R.id.btn_inv_lot);
        bSerie = (CardView) findViewById(R.id.btn_inv_serie);
        bExport = (CardView) findViewById(R.id.btn_export);
        bapropos = (CardView) findViewById(R.id.btn_apropos);
        bExit = (Button) findViewById(R.id.btn_exit);

        databaseHelper = new DatabaseHelper(this);



        bLot.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent activityInv = new Intent(MainActivity.this, InvLot.class);
                startActivity(activityInv);
                ;
            }
        });

        bSerie.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent activityInv = new Intent(MainActivity.this, InSerie.class);
                startActivity(activityInv);
                ;
            }
        });

        bExport.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent activityInv = new Intent(MainActivity.this, Export.class);
                startActivity(activityInv);
                ;
            }
        });

        bapropos.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent activityInv = new Intent(MainActivity.this, Propos.class);
                startActivity(activityInv);
                ;
            }
        });

        bExit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();
                System.exit(0);
            }
        });

        try {
            if (databaseHelper.EmptyArticleLot()&& databaseHelper.EmptyArticleSerie()) {
                Toast.makeText(MainActivity.this, "Aucun inventaire n'est disponible", Toast.LENGTH_LONG).show();
            }
            else {


                // Creating a new alert dialog to export file
                AlertDialog alert = new AlertDialog.Builder(this)
                        .setTitle("Un inventaire déjà existant, souhaitez vous le supprimer?")
                        .setPositiveButton("Oui",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                        dialog.dismiss();
                                        //faire appel à la méthode delete de la base pour supprimer le contenu des tables
                                        databaseHelper.delete_table_articleLot();
                                        databaseHelper.delete_table_articleSerie();
                                        Toast.makeText(MainActivity.this, "L'ancien inventaire est supprimé avec succès", Toast.LENGTH_LONG).show();


                                    }
                                })
                        .setNegativeButton("Non",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        // When you press cancel, just close the
                                        // dialog
                                        dialog.cancel();

                                    }
                                })
                        .show();




            }



        }

        catch (Exception e) {
            Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }




    }


}


