package com.example.inventaire;

import android.widget.TextView;

public class Article_lot {

    int id;
    String code;
    String lot;
    int quantite;

    // constructors
    public Article_lot() {
    }

    public Article_lot(int id, String code, String lot, int quantite) {
        this.id = id;
        this.code = code;
        this.lot = lot;
        this.quantite = quantite;}


    // setters
    public void setId(int id) {
        this.id = id;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public void setQuantite(int quantite){
        this.quantite = quantite;
    }


    // getters
    public int getId() {
        return this.id;
    }

    public String getCode() {
        return this.code;
    }

    public String getLot() {
        return this.lot;
    }

    public int getQuantite() {
        return this.quantite;
    }


}
